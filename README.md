#i-doit API Clients

The i-doit API Client is a library project for several programming languages connecting the API of [i-doit](http://www.i-doit.org) starting with [PHP](http://www.php.net).
We have the goal to provide an easy way to interact with the i-doit JSON-RPC API with this.

For more information see readmes of the corresponding API Client Directory or our [WIKI](https://bitbucket.org/dstuecken/i-doit-api-clients/wiki).