<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2015 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */

/**
 * Namespace aliases
 */
use idoit\Api\Client as ApiClient;
use idoit\Api\CMDB\Object as CMDBObject;
use idoit\Api\CMDB\CategoryConstants as Category;
use idoit\Api\CMDB\ObjectTypeConstants as ObjectType;
use idoit\Api\Connection as ApiConnection;

try
{
	/**
	 * i-doit
	 *
	 * Api usage example
	 *
	 * @package    i-doit
	 * @subpackage api
	 * @author     DS, 2014
	 */
	include_once(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'apiclient.php');
	include_once(__DIR__ . DIRECTORY_SEPARATOR . 'config.php');


	/* --------------------------------------------------------- */
	/* Initalize */
	/* --------------------------------------------------------- */
	$l_apiClient = new ApiClient(
		new ApiConnection(
			$api_entry_point, $api_key
		)
	);

	/* --------------------------------------------------------- */
	/* Test object creation */
	/* --------------------------------------------------------- */
	$l_objectApi = new CMDBObject($l_apiClient);
	$l_objectID  = $l_objectApi->getIdByTitle('Vertrag 1', ObjectType::TYPE_MAINTENANCE);

	$l_category     = new \idoit\Api\CMDB\Category($l_apiClient);

	/**
	 * Data
	 */
	$l_categoryData = new \idoit\Api\CMDB\Category\S\Contract();
	$l_categoryData->setData(
		array(
			'type'                    => '',
			'contract_no'             => 'text',
			'customer_no'             => 'text',
			'internal_no'             => 'text',
			'costs'                   => 'double',
			'product'                 => 'text',
			'reaction_rate'           => '',
			'contract_status'         => '',
			'start_date'              => '01.01.1999',
			'end_date'                => '01.01.2015',
			'run_time'                => '',
			'run_time_unit'           => '',
			'next_contract_end_date'  => '01.01.2015',
			'end_type'                => '',
			'next_notice_end_date'    => '01.01.2015',
			'notice_date'             => '01.01.2015',
			'notice_period'           => '',
			'notice_period_unit'      => '',
			'notice_type'             => '',
			'maintenance_period'      => '',
			'maintenance_period_unit' => '',
			'payment_period'          => '',
			'description'             => 'test description'
		)
	)->setDescription('changed');

	print_r(
		$l_category->add($l_objectID, $l_categoryData)
	);

}
catch (Exception $e)
{
	// Handle error message
	print_r($e->getMessage());
	echo "\n";
}