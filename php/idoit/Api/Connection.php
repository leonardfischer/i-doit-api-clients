<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2015 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */
namespace idoit\Api;

use idoit\Lib\Jsonrpc;

class Connection
{
	/**
	 * @var Jsonrpc
	 */
	public $jsonRpc = null;

	/**
	 * @var string
	 */
	public $apiKey = '';

    /**
     * @param $url
     * @param $apiKey
     *
     * @return Connection
     */
    public static function factory($url, $apiKey)
    {
        $instance = new self($url, $apiKey);

        return $instance;
    }

    /**
     * @param string $sessionId
     *
     * @return $this
     */
    public function setSessionId($sessionId)
    {
        $this->jsonRpc->setSessionId($sessionId);

        return $this;
    }

    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername($username)
    {
        $this->jsonRpc->setUsername($username);

        return $this;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->jsonRpc->setPassword($password);

        return $this;
    }

    /**
     * @param string $url
     * @param string $apiKey
     * @param string $username
     * @param string $password
     */
    public function __construct($url, $apiKey, $username = '', $password = '')
	{
		$this->jsonRpc  = new Jsonrpc($url, $username, $password, Config::$jsonRpcDebug);
		$this->apiKey   = $apiKey;
	}
}