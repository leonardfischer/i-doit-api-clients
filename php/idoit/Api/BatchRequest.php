<?php
/**
 * i-doit PHP API Client
 *
 * Copyright (c) 2015 Dennis Stücken
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   $Package$
 * @version   $Version$
 * @copyright Dennis Stücken
 * @author    Dennis Stücken <dstuecken@i-doit.com>
 * @license   http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 */
namespace idoit\Api;

class BatchRequest implements RequestInterface {

	/**
	 * Batch request queue
	 *
	 * @var Request[]
	 */
	private $requests = array();

	public function send()
	{
		$apiClient = $this->getApiClient();

		if ($apiClient && count($this->requests) > 0)
		{
			return $apiClient->connection->jsonRpc->batchRequest($this->requests);
		}

		throw new Exception('Error processing batch request: batch request queue');
	}

	/**
	 * @return Client
	 */
	public function getApiClient()
	{
		return (isset($this->requests[0])) ? $this->requests[0]->getApiClient() : NULL;
	}

	/**
	 * @param Request $request
	 */
	public function addRequest(Request $request)
	{
		$this->requests[] = $request;
	}

	/**
	 * Pass array of Request objects for payloading instead of using ->addRequest() for each one.
	 *
	 * @param Request[] $request
	 */
	public function __construct(/* ... */)
	{
		if (func_num_args() > 0)
		{
			foreach (func_get_args() as $request)
			{
				if ($request instanceof Request)
				{
					$this->requests[] = $request;
				}
			}
		}
	}

}